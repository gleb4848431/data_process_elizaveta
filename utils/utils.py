
import cv2
import PIL.Image

import os
import re
import tqdm
import glob
import shutil
import json

import albumentations as A
import numpy as np
import pandas as pd

def combine_cat_old(cat, dst):
    '''

    :param cat: folder with all image folders
    :param dst: dst folder with cloth and image folder
    :return:
    '''
    cloth = os.path.join(cat, 'cloth')
    dst_cloth = os.path.join(dst, 'cloth')
    dst_person = os.path.join(dst, 'image')

    os.makedirs(dst_person, exist_ok=True)
    os.makedirs(dst_cloth, exist_ok=True)

    # resize transformation
    # if working with masks interpolation=cv2.INTER_NEAREST
    transform = A.Resize(
        1024,
        768,
        always_apply=True,
        # for jpg
        interpolation=cv2.INTER_LINEAR
        # for png masks
        # interpolation=cv2.INTER_NEAREST

    )
    for folder in os.listdir(cat):
        if folder=='cloth':
            continue

        '''Every cloth image can have not more than 2 person images'''
        for _ in tqdm.tqdm(glob.glob(os.path.join(cat, folder, '*'))):
            # find numerical idx of image
            idx = _.split('/')[-1]
            id_item = re.findall('\d+', idx)[0]

            src_cloth = os.path.join(cloth, id_item + '_cloth.jpeg')
            save_cl = os.path.join(dst_cloth, id_item + '_00.jpg')


            src_person = _
            save_p = os.path.join(dst_person, id_item + '_00.jpg')

            '''
            If we already saved this (cloth + person) pair than save new version of this name (id_item + _2_00.jpg)
            else save _00.jpg
            '''
            if os.path.exists(save_p):
                save_p = os.path.join(dst_person, id_item + '_2_00.jpg')
                save_cl = os.path.join(dst_cloth, id_item + '_2_00.jpg')
                # if fails than we don't have a pair and can skip
                try:
                    p_im = PIL.Image.open(src_person)
                    cl_im = PIL.Image.open(src_cloth)
                    size_p = p_im.size
                    size_cl = cl_im.size
                except:
                    pass
                # check if width>=500 and height>=700
                if all([500 <= size_p[0], 700 <= size_p[1], 500 <= size_cl[0], 700 <= size_cl[1]]):
                    # resize images
                    p_im = transform(image=np.array(p_im))['image']
                    p_im = PIL.Image.fromarray(p_im)

                    cl_im = transform(image=np.array(cl_im))['image']
                    cl_im = PIL.Image.fromarray(cl_im)
                else:
                    continue

                p_im.save(save_p)
                cl_im.save(save_cl)


            else:
                # if fails than we don't have a pair and can skip
                try:
                    p_im = PIL.Image.open(src_person)
                    cl_im = PIL.Image.open(src_cloth)
                    size_p = p_im.size
                    size_cl = cl_im.size
                except:
                    pass
                # check if width>= 500 and height>=700
                if all([500 <= size_p[0], 700 <= size_p[1], 500 <= size_cl[0], 700 <= size_cl[1]]):
                    # resize images
                    p_im = transform(image=np.array(p_im))['image']
                    p_im = PIL.Image.fromarray(p_im)

                    cl_im = transform(image=np.array(cl_im))['image']
                    cl_im = PIL.Image.fromarray(cl_im)
                else:
                    continue

                p_im.save(save_p)
                cl_im.save(save_cl)
def delete_difference_agnistic(root, delete=False):
    '''
    delete everything that has some files skipped
    leave only those who have all files ready

    :param root: folder with cat folders (cat folder has to contain cloth folder at least)
    :param delete: delete files that don't have pair
    :return:
    '''
    set_total = set()
    for cat in os.listdir(root):
        folder_path = os.path.join(root, cat, 'cloth')
        cloth_images = set([_.split('_00')[0] for _ in os.listdir(folder_path)])
        for folder in os.listdir(os.path.join(root, cat)):
            folder_path = os.path.join(root, cat, folder)
            images = os.listdir(folder_path)
            cloth_images = cloth_images.intersection([_.split('_00')[0] for _ in images])
        split_sign = '_00'
        set_lowwest_images = cloth_images
        for folder in os.listdir(os.path.join(root, cat)):
            if folder in ['openpose_json']:
                suffix = '_00_keypoints.json'
            elif folder in ['image-parse-agnostic-v3.2', 'image-parse-v3']:
                suffix = '_00.png'
            elif folder in ['openpose_img']:
                suffix = '_00_rendered.png'
            else:
                suffix = '_00.jpg'
            folder_path = os.path.join(root, cat, folder)
            images = os.listdir(folder_path)
            names_to_remove = set([_.split(split_sign)[0] for _ in images]).difference(set_lowwest_images)
            if delete:
                for name in names_to_remove:
                    set_total.add(name)
                    # print(os.path.join(root, cat, folder, name+suffix))
                    os.remove(os.path.join(root, cat, folder, name+suffix))
            else:
                for name in names_to_remove:
                    set_total.add(name)
                    print(os.path.join(root, cat, folder, name+suffix))
                    # os.remove(os.path.join(root, cat, folder, name+suffix))
    print(len(set_total))
def sample_agnostic(root, num, add='_TEST', move_images=False, only_one_cat=False, new_root=False, new_cat=False, add_to_im_name=''):
    '''

    :param root:
    :param num: number to randomly sample
    :param add: add to new root name
    :param move_images:
    :param only_one_cat: if you want to sample from category not from root
    :param new_root:
    :param new_cat: if only_one_cat True
    :param add_to_im_name: add to every image idx
    :return:
    '''
    if not new_root:
        new_root = root+add
    else:
        pass
    for cat in os.listdir(root):
        if only_one_cat:
            cat = root.split('/')[-1]
            root = '/'.join(root.split('/')[:-1])
        idxs_from_folder = os.path.join(root, cat, 'image-parse-v3')
        idxs_all = [_.split('_00')[0] for _ in os.listdir(idxs_from_folder)]

        if len(idxs_all)>num:
            selected_idxs = pd.Series(idxs_all).sample(n=num).tolist()
        else:
            selected_idxs = idxs_all


        for folder in tqdm.tqdm(os.listdir(os.path.join(root, cat))):
            if folder in ['back', 'border', 'cloth_two', 'parse', 'parsing.csv', 'withs_of_cloth.csv']:
                continue
            if folder in ['openpose_json']:
                suffix = '_00_keypoints.json'

            elif folder in ['image-parse-agnostic-v3.2', 'image-parse-v3']:
                suffix = '_00.png'

            elif folder in ['openpose_img']:
                suffix = '_00_rendered.png'

            else:
                suffix = '_00.jpg'
            if only_one_cat and new_cat:
                new_folder_path = os.path.join(new_root, new_cat, folder)
            else:
                new_folder_path = os.path.join(new_root, cat, folder)

            os.makedirs(new_folder_path, exist_ok=True)

            if not move_images:
                for idx in selected_idxs:
                    try:
                        shutil.copy(os.path.join(root, cat, folder, idx+suffix),os.path.join(new_folder_path, add_to_im_name+idx+suffix),)
                    except:
                        pass
                    # if folder=='image-parse-v3':
                    #     shutil.copy(os.path.join(root, cat, folder, idx + '_00_new.png'),
                    #                 os.path.join(new_folder_path, idx + '_00_new.png'), )

            else:
                for idx in selected_idxs:
                    try:
                        shutil.move(os.path.join(root, cat, folder, idx+suffix),os.path.join(new_folder_path, add_to_im_name+idx+suffix),)
                    except:
                        pass
        if only_one_cat:
            return
def find_back_turned_with_pose(cat, move=False):
    '''
    copy people with back turned
    :param cat:
    :return:
    '''
    folder_to_copy_to = os.path.join(cat, 'back')
    try:
        os.mkdir(folder_to_copy_to)
    except:
        pass

    for pose in tqdm.tqdm(glob.glob(os.path.join(cat, 'openpose_json', '*.json'))):
        with open(pose, 'r') as f:
            pose_label = json.load(f)
            pose_label = pose_label['people'][0]['pose_keypoints_2d']
            x_2, x_5 = pose_label[2*3], pose_label[5*3]
        if x_2>x_5:
            jpg_name = pose.split('/')[-1].replace('_keypoints.json', '.jpg')
            shutil.copy(os.path.join(cat, 'image', jpg_name), os.path.join(folder_to_copy_to, jpg_name))
def del_if_labels(path, if_no_label=set(), label:int = ...):
    '''

    :param path: to parsing folder
    :param if_no_label: all labels from this set has to be in mask or delete
    :param label: delete if this label in mask (only if bool(if_no_label)==False)

    :return:
    '''
    if not if_no_label:
        for _ in tqdm.tqdm(glob.glob(os.path.join(path, '*.png'))):
            if label in set(np.unique(np.array(PIL.Image.open(_)))):
                os.remove(_)
    else:
        for _ in tqdm.tqdm(glob.glob(os.path.join(path, '*.png'))):
            if len(set(np.unique(np.array(PIL.Image.open(_)))).intersection(if_no_label))==0:
                os.remove(_)

if __name__ == '__main__':
    pass
    # cat_path = ''
    # dst_path = cat_path + '_converted'
    #
    # combine_cat_old(
    #     cat_path,
    #     dst_path
    # )